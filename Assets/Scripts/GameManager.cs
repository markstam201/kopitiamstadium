﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Runtime.Serialization;
[Serializable]
public class GameSettings{
	public float WAITER_SPEED = 0.5f;
	public float WAITER_STOP_MIN = -2.5f;
	public float WAITER_STOP_MAX = 2.5f;
	
	public float[] X_DUARATION = new float[]{1,1,1,1};
	public int[] MIN_NORMAL_SCENE = new int[]{5,4,3,2};
	public int[] MAX_NORMAL_SCENE = new int[]{8,7,6,5};
	public float[] WAITER_RESPAWN_MIN = new float[]{10,8,6,4};
	public float[] WAITER_RESPAWN_MAX = new float[]{12,10,8,6};
	public int[] NUMBER_BUTTON = new int[]{3,3,4,4};
	public bool[] FIX_BUTTON = new bool[]{true,true,false,false};
	public float[] EVENT_INTERVAL_MIN = new float[]{7,6,5,4};
	public float[] EVENT_INTERVAL_MAX = new float[]{9,8,7,6};
	public GameObject[] WaiterEasy = new GameObject[2];
	public GameObject[] WaiterNormal = new GameObject[2];
	public GameObject[] WaiterHard = new GameObject[3];
	public GameObject[] WaiterInsane = new GameObject[4];
}

public class GameManager : MonoBehaviour {

	public int CurrentLevel = 0;
	public GameObject TV;
	public GameSettings gameSettings;
	private int specialEventMileStone = 0;
	private int normalEventRequired = 0;
	private int currentEventIndex = 0;

	// Use this for initialization
	void Start () {
		gameSettings = new GameSettings ();
		CurrentLevel = 0;
		StartCoroutine ("ChangeEvent");
		//specialEventTrack = 0;
		CreateNumberNormalEvent ();
	}

	// Update is called once per frame
	void Update () {
		
	}

	IEnumerator ChangeEvent(){
		yield return new WaitForSeconds (gameSettings.X_DUARATION[CurrentLevel]);
		// change TV event;
		currentEventIndex++;
		OnChangeEvent ();

		StartCoroutine ("ChangeEvent");
	}
	void OnChangeEvent(){
		TVScreen tvScript = TV.GetComponent<TVScreen> ();

		// special Step 1 :
		if (specialEventMileStone == currentEventIndex) {
			// TO DO : Enable Button

			// TO DO : Change TV event to Special Event
			tvScript.ChangeScreenTV (true);

			// TO DO : Uncle react to Event
			Debug.Log("Special Event Step 1");
			return;
		}
		if (specialEventMileStone == currentEventIndex - 1) {
			// TO DO : Disable Button
			// Uncles angry if wrong
			Debug.Log("Special Event Step 2");
			return;
		}
		if (specialEventMileStone == currentEventIndex - 2) {
			// TO DO : Change TV event to Normal Event
			tvScript.ChangeScreenTV (false);

			// TO DO : Uncle to Neutral Pose

			CreateNumberNormalEvent();
			Debug.Log("Special Event End change to Normal Event ");
			return;
		}
		// TO DO : TV change to Normal Event
		Debug.Log("Normal Event");
		tvScript.ChangeScreenTV (false);
	}
	void CreateNumberNormalEvent(){
		normalEventRequired = UnityEngine.Random.Range (gameSettings.MIN_NORMAL_SCENE [CurrentLevel], gameSettings.MAX_NORMAL_SCENE [CurrentLevel]+1);
		specialEventMileStone = currentEventIndex + normalEventRequired;
	}


}


