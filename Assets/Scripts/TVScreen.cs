﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class TVScreen : MonoBehaviour {
	//float dem = 0;
	int currentScreen ;
	private bool _isSpecial;
	// Use this for initialization
	void Start () {
		currentScreen = -1;
	}
	
	// Update is called once per frame
	void Update () {
		//dem += Time.deltaTime;
		//if (dem >= 1) {
		//	ChangeScreenTV(Random.Range(0,2) > 0 ? true : false );
		//	dem = 0;
		//}
	}

	public void ChangeScreenTV(bool isSpecial){
		int number;
		int[] arr;
		if (isSpecial) {
			arr = new int[]{4,5,6,7,8,9,10,11,12};
		} else {
			arr = new int[]{13,14,15,16,17,18,19};
		}
		arr = arr.Where (val => val != currentScreen).ToArray ();
		number = arr [Random.Range (0, arr.Length)];
		currentScreen = number;
		var ren = GetComponents<SpriteRenderer> () [0];

		ren.sprite = Resources.Load<Sprite> ("Football Scene/S" + (number < 10 ? ("0" + number) : "" + number));
	}
}
